export const additional = {
  id: "foobar",
  property: "barfoo",
  _links: {
    self: {
      href: "http://example.org/api/foobar",
    },
  },
  _bem: [
    {
      block: "foobar-block",
      content: [
        {
          elem: "self-link",
          mix: {
            block: "link",
            attributes: {
              href: {
                $ref: "$['_links']['self']['href']",
              },
            },
          },
          content: ["content"],
        },
      ],
    },
  ],
};

export const valid = {
  _links: {
    self: {
      href: "http://example.org/api/foo",
    },
    root: {
      href: "http://example.org/api",
    },
  },
  id: "foo",
  property: "bar",
  _embedded: {
    related: [
      {
        _links: {
          self: {
            href: "http://example.org/api/biz",
          },
        },
        id: "biz",
        property: "baz",
        _bem: [
          {
            block: "foo-block",
            content: [
              "just some text",
              {
                block: "some-block",
                content: [
                  {
                    $ref: "$['id']",
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        _links: {
          self: {
            href: "http://example.org/api/qux",
          },
        },
        id: "qux",
        name: "lok",
        _bem: [
          {
            block: "foo-block",
            content: [
              "just some text",
              {
                elem: "some-elem",
                content: [
                  {
                    $ref: "$['id']",
                  },
                  {
                    block: "nested-block",
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
    additional: additional,
  },
  _bem: [
    {
      block: "home",
    },
    {
      $ref: "$['_embedded']['related'][0]['_bem']",
    },
    {
      $ref: "$['_embedded']['related'][1]['_bem']",
    },
    {
      $ref: "$['_embedded']['additional'][0]['_bem']",
    },
    "this will disappear",
  ],
};

export const noSelf = {
  _links: {
    canonical: {
      href: "http://example.org/api/foo",
    },
  },
};
