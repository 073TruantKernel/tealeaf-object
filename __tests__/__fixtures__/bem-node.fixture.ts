import BemJson from "../../src/bem-json";
import DynamicBemJson from "../../src/dynamic-bem-json";

export const dynamicContentBem: DynamicBemJson = {
  block: "test",
  content: {
    $ref: "$['id']",
  },
};

export const dynamicContentArrayBem: DynamicBemJson = {
  block: "test",
  content: [
    {
      $ref: "$['id']",
    },
  ],
};

export const simpleBem = {
  block: "exceptional",
  elem: "exceptionalElem",
  cls: "h-card",
  tag: "div",
  bem: false,
  js: true,
  mods: { name: "index" },
  elemMods: { type: "search" },
  content: ["just a string"],
};

export const dynamicBlockBem = {
  block: {
    $ref: "$['meta']['block']",
  },
};

export const dynamicElemBem = {
  elem: {
    $ref: "$['meta']['elem']",
  },
};

export const dynamicClsBem = {
  cls: {
    $ref: "$['meta']['cls']",
  },
};

export const dynamicBemBem = {
  bem: {
    $ref: "$['meta']['bem']",
  },
};

export const dynamicTagBem = {
  tag: {
    $ref: "$['meta']['tag']",
  },
};

export const dynamicJsBem = {
  js: {
    $ref: "$['meta']['js']",
  },
};

export const dynamicModsBem = {
  mods: {
    $ref: "$['meta']['mods']",
  },
};

export const dynamicElemModsBem = {
  elemMods: {
    $ref: "$['meta']['elemMods']",
  },
};

export const dynamicComplexModsBem = {
  mods: {
    name: { $ref: "$['meta']['mods']['name']" },
    type: "search",
  },
};

export const dynamicMixBem: DynamicBemJson = {
  block: "header",
  mix: [
    {
      block: "link",
      attributes: {
        id: "00001",
        "data-text": {
          block: "i-bem",
          elem: "i18n",
        },
        href: {
          $ref: "$['_links']['self']['href']",
        },
      },
    },
    {
      $ref: "$['meta']",
    },
  ],
};

export const bem5: BemJson = {
  block: "testBlock",
  content: ["just a string"],
};
