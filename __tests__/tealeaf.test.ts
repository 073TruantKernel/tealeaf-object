import { valid, additional } from "./__fixtures__/tealeaf.fixture";
import Tealeaf from "../src/tealeaf";
import BemNode from "../src/bem-node";
import HALHyperlink from "hal-object/lib/cjs/hal-hyperlink";
let tealeaf: Tealeaf;

describe("Tealeaf Class", function () {
  test("Tealeaf instantiable", () => {
    expect((tealeaf = new Tealeaf(valid)));
  });

  test("root Tealeaf contains data", () => {
    expect(tealeaf.getData()).toStrictEqual({ id: "foo", property: "bar" });
  });

  test("root Tealeaf contains link to self", () => {
    expect((tealeaf.getLinks("self") as HALHyperlink[])[0]).toStrictEqual({
      href: "http://example.org/api/foo",
    });
  });

  test("Tealeaf can get link to self with getSelfLink", () => {
    expect(tealeaf.getSelfLink()).toStrictEqual({
      href: "http://example.org/api/foo",
    });
  });

  test("Tealeaf can get specified embedded Tealeaf", () => {
    expect(
      (tealeaf.getEmbedded("additional") as Array<Tealeaf>)[0]
    ).toStrictEqual(new Tealeaf(additional));
  });

  test("Tealeaf can resolve JsonPaths", () => {
    expect((tealeaf._bem as BemNode[])[0].resolve());
  });
});
