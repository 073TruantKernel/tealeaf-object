import { isBemJson } from "../src/bem-json";

describe("BemJson Interface", function () {
  test("isBemJson returns true when the object has either a block or element class", () => {
    expect(isBemJson({ block: "someBlock" })).toBeTruthy;
    expect(isBemJson({ elem: "someElem" })).toBeTruthy;
  });

  test("isBemJson returns false when the object has neither a block nor element class", () => {
    expect(isBemJson({ NothingThatHasToDoWithBem: "This is just text" }))
      .toBeFalsy;
  });
});
