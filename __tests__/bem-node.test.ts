import * as Fixtures from "./__fixtures__/bem-node.fixture";
import * as Props from "./__fixtures__/props.fixture";
import BemNode from "../src/bem-node";
import BemJson from "../src/bem-json";
import DynamicBemJson from "../src/dynamic-bem-json";

describe("BemNode class", function () {
  test("BemNode fully instantiable", () => {
    expect(new BemNode(Fixtures.simpleBem as BemJson).resolve()).toStrictEqual(
      Fixtures.simpleBem
    );
  });

  test("BemNode can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicContentArrayBem as BemJson,
        Props.initialContentProps
      ).resolve()
    ).toStrictEqual({
      block: "test",
      content: ["foo"],
    });
  });

  test("dynamic block can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicBlockBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      block: "exceptional",
    });
  });

  test("dynamic elem can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicElemBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      elem: "exceptionalElem",
    });
  });

  test("dynamic cls can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicClsBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      cls: "h-card",
    });
  });

  test("dynamic tag can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicTagBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      tag: "div",
    });
  });

  test("dynamic bem can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicBemBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      bem: false,
    });
  });

  test("dynamic js can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicJsBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      js: true,
    });
  });

  test("dynamic mods can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicModsBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      mods: { name: "index" },
    });
  });

  test("dynamic elemMods can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicElemModsBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      elemMods: { type: "search" },
    });
  });

  test("dynamic complex mods can be resolved", () => {
    expect(
      new BemNode(
        Fixtures.dynamicComplexModsBem as DynamicBemJson,
        Props.dynamicProps
      ).resolve()
    ).toStrictEqual({
      mods: {
        name: "index",
        type: "search",
      },
    });
  });

  test("BemNode can be updated", () => {
    const node = new BemNode(
      Fixtures.dynamicContentBem as BemJson,
      Props.initialContentProps
    );
    expect(node.resolve()).toStrictEqual({ block: "test", content: "foo" });
    node.setProps(Props.finalContentProps);
    expect(node.getProps()).toStrictEqual(Props.finalContentProps);
    expect(node.resolve()).toStrictEqual({ block: "test", content: "bar" });
  });

  test("dynamic mix can be resolved", () => {
    const node = new BemNode(
      Fixtures.dynamicMixBem as BemJson,
      Props.dynamicMixBemProps
    );
    expect(node.resolve().mix).toStrictEqual([
      {
        block: "link",
        attributes: {
          id: "00001",
          "data-text": {
            block: "i-bem",
            elem: "i18n",
          },
          href: "https://localhost/api/",
        },
      },
      {
        block: "exceptional",
      },
    ]);
  });
});
