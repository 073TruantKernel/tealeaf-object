import { HALO, CHALO } from "cacheable-hal-object";
import BemJson from "./bem-json";
import { JSONPath } from "jsonpath-plus";
import BemNode from "./bem-node";

export default class Tealeaf extends CHALO {
  public _bem?: Array<BemNode>;
  public static readonly bemProperyName = "_bem";
  protected static readonly jsonPathReference = "$ref";

  public constructor(json: any) {
    super(json);
  }

  public getProps() {
    const {
      _bem: _1, // DROP
      _request: _2, // DROP
      _response: _3, // DROP
      _embedded: _4, // DROP
      data: dto,
      ...props // REST
    } = this;
    const obj = { ...dto, ...props };
    return obj;
  }

  protected init(json: any) {
    JSON.parse(JSON.stringify(json), (key, value) => {
      if (key === HALO.embeddedProperyName) {
        this._embedded = {};
        for (const rel in value) {
          const related = value[rel];
          if (Array.isArray(related)) {
            this._embedded[rel] = new Array<Tealeaf>();
            for (let i = 0; i < value[rel].length; i++) {
              (this._embedded[rel] as Tealeaf[])[i] = new Tealeaf(related[i]);
            }
          } else {
            this._embedded[rel] = new Array<Tealeaf>(new Tealeaf(related));
          }
        }
      }
      this.reviveBem(key, value, json);
      return value;
    });
  }

  protected reviveBem(key: string, value: any, source: any): void {
    if (key !== Tealeaf.bemProperyName) {
      return;
    }

    this._bem = new Array<BemNode>();
    for (let i = 0; i < value.length; i++) {
      if (value[i].block != null || value[i].elem != null) {
        this._bem.push(
          new BemNode(value[i] as BemJson, this.getProps() as any)
        );
      } else {
        if (value[i][Tealeaf.jsonPathReference]) {
          const jsonPath: string = value[i][Tealeaf.jsonPathReference];
          let test: BemJson = (JSONPath({
            path: jsonPath,
            json: source,
          }) as BemJson[])[0];
          test = Array.isArray(test) ? (test[0] as BemJson) : test; //DROP ARRAY FOR NOW, FIX LATER :)
          const path: Array<string> = (JSONPath as any).toPathArray(
            value[i][Tealeaf.jsonPathReference]
          );
          const index = path.indexOf(HALO.embeddedProperyName) + 1;
          const embedded = this.getEmbedded(path[index]) as Tealeaf[];
          const props = embedded[Number.parseInt(path[index + 1])].getProps();
          if (test != null) {
            this._bem.push(new BemNode(test as BemJson, props as any));
          }
        }
      }
    }
  }
}
